<?php

namespace Bestloc\Drivers;

class FTP {
    public $base_dir = null;
    public $host = null;
    public $user = null;
    public $pass = null;
    public $port = null;
    public $conn = null;

    public function __construct($base_dir = '/', $host, $user, $pass, $port = 21) {
        $this->setBaseDir($base_dir)
            ->setHost($host)
            ->setUser($user)
            ->setPass($pass)
            ->setPort($port);
    }

    public function setBaseDir($base_dir) {
        $this->base_dir = $base_dir;
        return $this;
    }

    public function getBaseDir() {
        return '/'.trim($this->base_dir,'/');
    }

    public function setHost($host) {
        $this->host = $host;
        return $this;
    }

    public function getHost() {
        return $this->host;
    }

    public function setUser($user) {
        $this->user = $user;
        return $this;
    }

    public function getUser() {
        return $this->user;
    }

    public function setPass($pass) {
        $this->pass = $pass;
        return $this;
    }

    public function getPass() {
        return $this->pass;
    }

    public function setPort($port = 21) {
        $this->port = $port;
        return $this;
    }

    public function getPort() {
        return $this->port;
    }

    public function getConn() {
        if (null == $this->conn) {
            $this->connect();
        }
        return $this->conn;
    }

    public function connect() {
        $conn = ftp_connect($this->getHost(), $this->getPort());
        if (FALSE === $conn) {
            throw new \Exception("Não foi possível connectar no FTP.");
        }
        if (!ftp_login($conn, $this->getUser(), $this->getPass())) {
            throw new \Exception("Não foi possível fazer login no FTP.");
        }     
        
        ftp_pasv($conn, true);

        /*
        register_shutdown_function(function() use ($conn) {
            if ($conn) {
                ftp_close($conn);
            }
        });
        */
        $this->conn = $conn;
    }

    public function disconnect() {
        if ($this->conn) {
            ftp_close($this->conn);
        }
    }

    public function ls($path = '.') {
        $conn = $this->getConn();

        $dirname = $this->getBaseDir() . '/' . trim($path, '/');
        $contents = ftp_nlist($conn, $dirname);

        $contents = array_filter($contents, function($file) {
            $file = basename($file);
            return !in_array($file, array('.','..'));
        });
        $files = array_map(function($file) use($path) {
            return '/'.trim($path,'/').'/'. basename($file);
        }, $contents);
		return $files;
    }

    public function writeFile($path, $content) {
        $conn = $this->getConn();

        if (preg_match('/\.(\w{3,4})$/',$content,$m)) {
			$origem = $content;
		} else {
			$tmp_dir = sys_get_temp_dir();
			$origem = tempnam($tmp_dir,'ftp');
			file_put_contents($origem,$content);
        }
        
        $filename = $this->getBaseDir() . '/'. trim($path, '/');

        if (!ftp_put($conn, $filename, $origem, FTP_BINARY)) {
			throw new \Exception("Não foi possível gravar o arquivo.");
		}

        return $this;
    }

    public function readFile($path, $destino = null) {
        $conn = $this->getConn();

		$return_content = false;
		if (null == $destino) {
			$tmp_dir = sys_get_temp_dir();
			$destino = tempnam($tmp_dir,'ftp');
			$return_content = true;
		}

        $filename = $this->getBaseDir() . '/'. trim($path, '/');

		if (!ftp_get($conn, $destino, $filename, FTP_BINARY)) {
			throw new \Exception("Não foi possível ler o arquivo.");
		}
		if (!$return_content) return true;
		return file_get_contents($destino);
    }

    public function move($from, $to) {
        $conn = $this->getConn();

        $from_path = $this->getBaseDir() . '/' .trim($from,'/');
        $to_path = $this->getBaseDir() . '/' .trim($to,'/');

        return ftp_rename($conn, $from_path, $to_path);
    }

    public function remove($path) {
        $conn = $this->getConn();

        $filename = $this->getBaseDir() . '/'. trim($path, '/');

        return ftp_delete($conn, $path);
    }
}