<?php

namespace Bestloc;

class Imagens {
    public static $records = null;
    
    public static function getFiles() {
        $files = ERP::getFiles();
        $arquivos = array();
        foreach($files as $filename) {
            if (!preg_match('/\/(?P<imagem_id>\d+)\.jpg$/', $filename, $matches)) {
                continue;
            }
            $imagem_id = (int)$matches['imagem_id'];

            $arquivo = new \stdclass();
            $arquivo->filename = $filename;
            $arquivo->imagem_id = $imagem_id;
            $arquivos[] = $arquivo;
        }
        return $arquivos;
    }

    public static function getRecords() {
        if (null == static::$records) {
            static::$records = static::getFiles();
        }
        return static::$records;
    }
}