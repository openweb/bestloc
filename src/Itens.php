<?php

namespace Bestloc;

class Itens {
    public static $records = null;
    public static $lastVersion = null;

    public static function clear() {
        static::$records = null;
    }

    public static function getVersion() {
        if (null == static::$lastVersion) {
            static::$lastVersion = ERP::getVersion('itens');
        }
        return static::$lastVersion;
    }

    public static function setVersion($version) {
        static::$lastVersion = $version;
    }

    public static function getFiles() {
        $files = ERP::getFiles();
        $lastVersion = static::getVersion();
        $arquivos = array();        

        foreach($files as $filename) {
            if (!preg_match('/\/Itens_(?P<versao>\d+)\.xml$/', $filename, $matches)) {
                continue;
            }
            $versao = (int)$matches['versao'];
            if ($versao <= $lastVersion) {
                continue;
            }

            $arquivo = new \stdclass();
            $arquivo->filename = $filename;
            $arquivo->versao = $versao;
            $arquivos[] = $arquivo;

            if ($versao > $lastVersion) {
                $lastVersion = $versao;
            }
        }
        
        static::setVersion($lastVersion);
        return $arquivos;
    }

    public static function getRecords() {
        if (null == static::$records) {
            static::$records = static::generate();
        }
        return static::$records;
    }

    public static function generate() {
        $files = static::getFiles();
        $records = array();

        foreach($files as $file) {
            $content = FS::readFile($file->filename);
            $rows = Parser::parse($content);
            if (null == $rows) {
                continue;
            }

            foreach($rows as $row) {
                $record = new \stdclass();
                $record->id = (int)$row->item_kit_id;
                $record->item_id = (int)$row->item_id;
                $record->imagem = (int)$row->imagem_id.'.jpg';
                $record->titulo = $row->descricaoautomatica;
                $record->observacoes = $row->observacoes;
                $record->exibir_site = (int)$row->exibirnosite;
                $record->ativo = (int)$row->ativo;
                $record->personalizavel = (int)$row->personalizavel;
                $record->valor_locacao = (double)str_replace(',','.',str_replace('.','',$row->valor_locacao));
                $record->valor_reposicao = (double)str_replace(',','.',str_replace('.','',$row->valor_reposicao));
                $record->estoque = (int)$row->qtde_acervo;
                $record->versao = $file->versao;
                if (0 == (int)$row->imagem_id) {
                    $record->imagem = '';
                }
                $records[$record->id] = $record;
            }
        }

        return $records;
    }
}