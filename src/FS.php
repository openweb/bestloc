<?php

namespace Bestloc;

class FS {
    public static $driver = null;

    public static function getDriver() {
        if (null == static::$driver) {
            static::setDriver(new Drivers\Filesystem($_SERVER['DOCUMENT_ROOT']));
        }
        return static::$driver;
    }

    public static function setDriver($driver) {
        static::$driver = $driver;
    }

    public static function ls($path) {
        $driver = static::getDriver();
        return $driver->ls($path);
    }

    public static function writeFile($path, $data) {
        $driver = static::getDriver();
        return $driver->writeFile($path, $data);
    }

    public static function readFile($path, $destino = null) {
        $driver = static::getDriver();
        return $driver->readFile($path, $destino);
    }

    public static function move($from, $to) {
        $driver = static::getDriver();
        return $driver->move($from, $to);
    }

    public static function remove($path) {
        $driver = static::getDriver();
        return $driver->remove($path);
    }
}