<?php

namespace Bestloc;

class ItensLinha {
    public static $records = null;
    public static $lastVersion = null;

    public static function clear() {
        static::$records = null;
    }

    public static function getVersion() {
        if (null == static::$lastVersion) {
            static::$lastVersion = ERP::getVersion('itenslinha');
        }
        return static::$lastVersion;
    }

    public static function setVersion($version) {
        static::$lastVersion = $version;
    }
    
    public static function getFiles() {
        $files = ERP::getFiles();
        $lastVersion = static::getVersion();

        $arquivos = array();

        foreach($files as $filename) {
            if (!preg_match('/\/ItensLinha_(?P<versao>\d+)\.xml$/', $filename, $matches)) {
                continue;
            }
            $versao = (int)$matches['versao'];
            if ($versao <= $lastVersion) {
                continue;
            }

            $arquivo = new \stdclass();
            $arquivo->filename = $filename;
            $arquivo->versao = $versao;
            $arquivos[] = $arquivo;

            if ($versao > $lastVersion) {
                $lastVersion = $versao;
            }
        }

        static::setVersion($lastVersion);
        return $arquivos;
    }

    public static function getRecords() {
        if (null == static::$records) {
            static::$records = static::generate();
        }
        return static::$records;
    }

    public static function generate() {
        $files = static::getFiles();
        $records = array();

        foreach($files as $file) {
            $content = FS::readFile($file->filename);
            $rows = Parser::parse($content);
            if (null == $rows) {
                continue;
            }

            foreach($rows as $row) {
                $record = new \stdclass();
                $record->id = (int)$row->itemxlinha_id;
                $record->item_id = (int)$row->item_id;
                $record->linha_id = (int)$row->linha_id;
                $record->sincronismo_id = (int)$row->sincronismo_id;
                $record->kit_id = (int)$row->kit_id;
                $record->item_kit_id = (int)$row->item_kit_id;
                $record->excluido = (int)$row->excluido;                
                $record->versao = $file->versao;
                               
                $records[$record->id] = $record;
            }
        }

        return $records;
    }
}