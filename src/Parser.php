<?php

namespace Bestloc;

class Parser {
    public static function codificacao($string) {
		return mb_detect_encoding($string.'x', 'UTF-8, ISO-8859-1');
	}
	
	public static function toUTF($var) {
		if (!is_array($var) && !is_object($var)) {
			if (static::codificacao($var)!='UTF-8') {
				$var = utf8_encode($var);
			}
			} else if (!is_object($var)){
			foreach($var as $key=>$value) {
				$var[$key] = static::toUTF($value);
			}
			} else {
			foreach((array)$var as $key=>$value) {
				$var->{$key} = static::toUTF($value);
			}
		}
		return $var;
	}
    public static function parse($content) {
        if ('' == $content) {
            return null;
        }
        $data = array();
        $previous = libxml_use_internal_errors(true);
        $content = str_replace('Windows-1252','utf-8', $content);
        $content = static::toUTF($content);
        $content = preg_replace_callback('/\=\"(?P<content>[^\"]+)\"/',function($matches) {
            return '="'.htmlspecialchars($matches['content']).'"';
            //return '="'.htmlentities($matches['content'], ENT_QUOTES| ENT_XHTML,'cp1252').'"';
        }, $content);
 
        $xml = simplexml_load_string($content);
        if (!$xml) { 
            $message = '';
            foreach (libxml_get_errors() as $error) {
                $message .= trim($error->message)
                . " on line: $error->line, column: $error->column.\n";
            }
            libxml_clear_errors();
            libxml_use_internal_errors($previous);
            throw new \Exception($message);
        }
 
        libxml_use_internal_errors($previous);
        foreach($xml->ROWDATA->ROW as $row) {

            $attr = $row->attributes();
            $d = json_decode(json_encode($attr))->{"@attributes"};
            $d2 = array();
            foreach((array)$d as $key=>$value) {
                $d2[strtolower($key)] = trim($value);
            }
            $data[] = (object)static::toUTF($d2);
        }
 
        return $data;
    }
}