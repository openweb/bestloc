<?php

namespace Bestloc\Drivers;

class Filesystem {
    public $base_dir = null;
    public function __construct($base_dir = '/') {
        $this->setBaseDir($base_dir);
    }

    public function setBaseDir($base_dir) {
        $this->base_dir = $base_dir;
        return $this;
    }

    public function getBaseDir() {
        return '/'.trim($this->base_dir,'/');
    }

    public function ls($path = '/') {
        $dir = $this->getBaseDir().'/'.trim($path,'/');
        $files = scandir($dir);
        $files = array_filter($files, function($file) {
            if (in_array($file, array('.','..'))) {
                return false;
            }
            return true;
        });
        $files = array_map(function($file) use ($path) {
            return '/'.trim($path).'/'.trim($file,'/');
        }, $files);
        return $files;
    }

    public function writeFile($path, $data) {
        $filename = $this->getBaseDir() . '/' . trim($path,'/');
        file_put_contents($filename, $data);
        return $this;
    }

    public function readFile($path, $destino = null) {
        $filename = $this->getBaseDir() . '/' . trim($path,'/');

        if (!file_exists($filename)) {
            throw new \Exception("Não foi possível ler o arquivo: {$filename}.");
        }
        $data = file_get_contents($filename);
        if (null == $destino) {
            return $data;
        }

        file_put_contents($destino, $data);
        return true;
    }

    public function move($from, $to) {
        $from_path = $this->getBaseDir() . '/' .trim($from,'/');
        $to_path = $this->getBaseDir() . '/' .trim($to,'/');

        return rename($from_path, $to_path);
    }

    public function remove($path) {
        $filename = $this->getBaseDir() . '/' . trim($path,'/');
        return @unlink($filename);
    }
}