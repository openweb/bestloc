<?php

namespace Bestloc;

class ERP {

    public static $files = null;
    public static function getFiles() {
        if (null == static::$files) {
            $files = FS::ls('erp_to_web');
            uasort($files, function($a, $b) {
                return $a < $b ? -1 : 1;
            });
            static::$files = $files;
        }
        return static::$files;
    }

    public static function clear() {
        static::$files = null;
    }

    public static function sync($arquivo, $data) {
        FS::writeFile("web_to_erp/${arquivo}.xml", $data);
    }

    public static $versions = null;
    public static function getVersions() {
        if (null == static::$versions) {
            try {
                $data = FS::readFile("versions.json");
                $data = (object)json_decode($data);
                static::$versions = $data;
            } catch (\Exception $e) {
                static::$versions = new \stdclass();
            }
        }
        return static::$versions;
    }

    public static function getVersion($type) {
        $versions = static::getVersions();
        return isset($versions->{$type}) ? (int) $versions->{$type} : 0;
    }

    public static function setVersion($type, $version) {
        $versions = static::getVersions();
        $versions->{$type} = (int)$version;
        FS::writeFile('versions.json', json_encode($versions));
    }
}